# Neovim Config

This is my Neovim config. To install this on your machine, you need to clone this repository into the standard path of your configuration. First install Neovim on your machine using the very helpful guide on their [website](https://github.com/neovim/neovim/blob/master/INSTALL.md). Then run the following in command mode in Neovim.

```
:echo stdpath('config')
```

Navigate to this location and clone this repository to replace the nvim folder with this one. My configuration is simple. It just does some basic configuration of the tabs, line numbers. However, there are additional steps that need to be followed to install plugins. For this, please follow the documentation [here](https://github.com/junegunn/vim-plug). Then install the plugins. Once done, we will also need to install the LSP server for the Markdown format which can be done by using Mason and Lspconfig.
