-- Basic configuration
local basic = vim.opt

-- Line number display (Show current line as is and other line number relative to it)
basic.number = true 
basic.relativenumber = true

-- Tab definitions
basic.tabstop = 4
basic.shiftwidth = 4

-- Disable the creation of swapfiles for cleaner directories
basic.swapfile = false

-- Wrap better
basic.linebreak = true

-- Plugins
local Plug = vim.fn['plug#']
vim.call('plug#begin')
-- LSP Servers
Plug 'williamboman/mason.nvim'
Plug 'williamboman/mason-lspconfig.nvim'
Plug 'neovim/nvim-lspconfig'
-- Distraction free writing
Plug 'junegunn/goyo.vim'
vim.call('plug#end')

-- Setup Mason
require("mason").setup()
require("mason-lspconfig").setup()
require("lspconfig").ltex.setup{}
